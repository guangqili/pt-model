############ subfunction of P model\n
# using the delta13C equation to estimate chi\n
source("sub_pt/chi_source.r")

chi_simple <- function (tc, elv, vpd) {
	####################1
	patm <- calc_patm( elv )  ### unite of elv is meter
	kmm  <- calc_k( tc, patm )* ( 1.e6 ) / patm  #### tc degree c, patm : Pa, kmm : ppm
 	####################2
	ns      <- viscosity_h2o( tc, patm )  # Pa s 
	ns25    <- viscosity_h2o( kTo, kPo )  # Pa s 
	ns_star <- ns / ns25  # (unitless)
	####################4
	xi  <- sqrt( (beta * kmm ) / ( 1.6 * ns_star ) )
	vpd_ppm <- vpd * ( 1.e6 ) / patm
	chi <- xi / (xi + sqrt(vpd_ppm))
	chi
}

# estimate CO2 compensation point\n
CO2.compens.point <- function(temp) {
	gammastar25 = 42.75
	k = 0.0512
	gammastar <- gammastar25 * exp(k * (temp - 25))
	return(gammastar)
}

#### major function of P model
P1 <- function(epsilon, ele, ca, address) {
	# inputs\n
	SeaWiFS.fAPAR <- 1
	elv <- ele * 0.001 ### the elevation of the simulated sites
	temp <- read.csv(paste(address,"cru_tas.csv",sep=''))[,2:13]   ## CRU3.1 need to change with value-273.15
	vpd <- read.csv(paste(address,"cru_vpd.csv",sep=''))[,2:13] *100      ## CRU3.1 need to change with value-273.15
	vpd[vpd<0]=0
	maxQE <- epsilon * 0.8 * 12 # unit: mol C/ mol photon                                 
	mPAR0 <- read.csv(paste(address,"mpar.csv",sep=''))[, 2:13]
	mpet <- read.csv(paste(address,"mpet.csv",sep=''))[, 2:13]
	malpha <- read.csv(paste(address,"malpha.csv",sep=''))[, 2:13]*1.26
	gammastar <- CO2.compens.point(temp)
	ci <- chi_simple (temp, elv = ele, vpd) * ca
	co2.lim <- (ci - gammastar)/(ci + 2 * gammastar)

	# estimate GPP\n
	Gmodel <- function(fAPAR, PAR, co2.lim) {
		Iabs <- fAPAR * PAR
		GPP <- maxQE * Iabs * co2.lim
	}
	# estimating GPP\n
	GPP.co2lim <- Gmodel(fAPAR = SeaWiFS.fAPAR, PAR = mPAR0 * ((malpha/1.26) ^0.25), co2.lim = co2.lim)  ### use the old no ^0.25 
	return(GPP.co2lim)

}
