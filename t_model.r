﻿### T model was first published at 2014. Details can be found at the following paper
### Li, G., Har_rootison, S. P., Colin Prentice, I., & Falster D. 2014. Simulation of tree ring-widths with a model for primary production, carbon allocation and growth. Biogeosciences, 11, 6711–6724. doi:10.5194/bg-11-6711-2014

### input data includes potential GPP data from 'P' model (gpp, unit: kg C m-2 year-1), and species spicific parameters

# a: initial slope of height–diameter relationship (–)
# cr: Initial ratio of crown area to stem cross-sectional area (–) 
# Hm: Maximum tree height (m)
# ρs (rho): Sapwood density (kgCm−3)
# L: Leaf area index within the crown (–)
# σ (sigma): Specific leaf area (m2 kg−1C)
# τf (tau_foliage): Foliage turnover time (years)
# τr (tau_root): Fine-root turnover time (years)
# K: PAR extinction coefficient (–)
# y: Yield factor (–)
# ζ (zeta): Ratio of fine-root mass to foliage area (kgCm−2)
# rr (r_root): Fine-root specific respiration rate (year−1)
# rs (r_sapwood): Sapwood-specific respiration rate (year−1)

# And model need the initial tree size the diameter (D), with the unit of 'm'

# Model output will be the increment of dimater (m). To transfer it into ring width, "dD/2*1000" is needed

tmodel <- function(gpp, year, a, cr, Hm, rho, r_root, r_sapwood, L, zeta, y, sigma, tau_foliage, tau_root, D, K) {
	gpp <- gpp * (1 - 0.1) ### remove foliage respiration by 10% of the gpp
	year_no <- length(year) ### years for output
	output <- c() ### blank vector for the output
	dD <- 0
	NPP1 <- NA
	for (i in 1:year_no) {
		D <- D + dD ### tree stem diameter
		H <- Hm * (1 - exp(-a * D/Hm)) ### tree height
		fc <- H/(a * D) ### crown fraction
		dWs <- pi/8 * rho * D * (a * D * (1 - (H/Hm)) + 2 * H) ### stem wood increment
		Ac <- ((pi * cr)/(4 * a)) * D * H ### crown area
		Ws <- (pi/8) * (D^2) * H * rho ### stem wood biomass
		Wf <- Ac * L * (sigma^(-1)) ### foliage biomass
		Wss <- Ac * rho * H * (1 - fc/2)/cr ### sapwood biomass
		GPP <- (Ac * gpp[i] * (1 - exp(-(K * L)))) ### GPP absorbed by the crown, patitioned by the Leaf area index within the crown (L)
		Rm1 <- Wss * r_sapwood ### sapwood respiration
		Rm2 <- zeta * sigma * Wf * r_root	### fine root res
		NPP <- y * (GPP - Rm1 - Rm2)  ### NPP, after removing the respiration of sapwood and fine root (and foliage) from the GPP
		NPP1 <- NPP ### carryover (%) can be set here as NPP * (1- carrover)
		NPP2 <- (Ac * L * ((1/(sigma * tau_foliage)) + (zeta/tau_root))) ### turnover of foliage and roo
		NPP3 <- (L * ((pi * cr)/(4 * a)) * (a * D * (1 - (H/Hm)) + H) * (1/sigma + zeta))
		dD <- (NPP1 - NPP2)/(NPP3 + dWs) ### increment of diameter
		dWs <- (pi/8 * rho * D * (a * D * (1 - (H/Hm)) + 2 * H)) * dD ### increment of stem wood biomass
		dWfr <- (L * ((pi * cr)/(4 * a)) * (a * D * (1 - (H/Hm)) + H) * (1/sigma + zeta)) * dD ### increment of foliage biomass
		output<- c(output, as.numeric(dD))
	}
	return (output)
}
